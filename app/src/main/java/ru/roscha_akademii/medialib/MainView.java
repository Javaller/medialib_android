package ru.roscha_akademii.medialib;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by tse on 09/09/16.
 */
public interface MainView extends MvpView {
    void showHelloToast();
}
